<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if(!CModule::IncludeModule("form"))
	return;
	
	$rsForm = CForm::GetList("s_sort", "asc");
	$arFormReady = [];

	while ($arForm = $rsForm->Fetch()) {
		$arFormReady[$arForm["ID"]] = "[".$arForm["ID"]."] ".$arForm["NAME"];
	}

$arComponentParameters = array(
	"PARAMETERS" => array(
        "FORM" => array(
			"PARENT" => "BASE",
			"NAME" => "Выбрать форму",
			"TYPE" => "LIST",
			"VALUES" => $arFormReady,
			"REFRESH" => "Y",
			"DEFAULT" => "",
		),
		"BUTTON_ID" => array(
			"PARENT" => "BASE",
			"NAME" => "ID кнопки выозова",
			"TYPE" => "STRING",
			"DEFAULT" => "",
		)
    )
);
?>