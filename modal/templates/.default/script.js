window.addEventListener("DOMContentLoaded", () => {
    let modal = document.querySelector(".modal");
    const json = document.querySelector("#sessionsData");
    const formBtn = document.querySelector(".modal__container__form>form>input[type=button]");
    const formInputs = document.querySelectorAll(".modal__container__form>form>input[type=text]");
    const callForm = document.querySelector(`#${JSON.parse(json.textContent).btnId}`);
    const btnCloseModal = document.querySelector(".modal__container__close>i");

    callForm.addEventListener("click", () => {
        modal.classList.add("modal_js");
    })

    formBtn.addEventListener("click", () => {
        const request = new XMLHttpRequest();
        const url = JSON.parse(json.textContent).url;
        let params = "";

        for (let i = 0; i < formInputs.length; i++) {
            if (i == 0) {
                params += `${formInputs[i].getAttribute("name")}=${formInputs[i].value}`;
            } else {
                params += `&${formInputs[i].getAttribute("name")}=${formInputs[i].value}`;
            }
        }
        
        request.responseType =  "json";
        request.open("POST", url, true);
        request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        
        request.addEventListener("readystatechange", () => {
        
            if (request.readyState === 4 && request.status === 200) {
            //     let obj = request.response;
            // console.log(obj);       
            // // Здесь мы можем обращаться к свойству объекта и получать  его значение
            // console.log(obj.id_product);
            // console.log(obj.qty_product);   
            }
        });

        request.send(params);

        // if (formUserPhone.value != "") {
        //     modalThanks.classList.add("modalThanks_js");
        //     modal.classList.remove("modal_js");
        //     request.send(params);
        // } else {
        //     formUserPhone.classList.add("error");
        // }
    })

    btnCloseModal.addEventListener("click", () => {
        modal.classList.remove("modal_js");
    })
});