<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

?>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.4/css/all.css" integrity="sha384-DyZ88mC6Up2uqS4h/KRgHuoeGwBcD4Ng9SiP4dIRy0EXTlnuz47vAwmeGwVChigm" crossorigin="anonymous">

<!-- <pre>
    <?print_r($arResult["ITEMS"])?>
</pre> -->

<!-- <pre>
    <?print_r($arParams)?>
</pre> -->

<div class="modal">
    <div class="modal__container">
        <div class="modal__container__close">
            <i class="fas fa-times"></i>
        </div>

        <div class="modal__container__form">
            <form action="#" method="post">
                <?foreach($arResult["ITEMS"] as $input):?>
                    <input type="text" name="<?=$input["SID"]?>" placeholder="<?=$input["TITLE"]?>"> <br>
                <?endforeach;?>

                <input type="button" value="Отправить" id="send">
            </form>
        </div>
    </div>
</div>

<script id="sessionsData" type="application/json">
    {
        "btnId": "<?=$arParams['BUTTON_ID']?>",
        "url": "<?=$APPLICATION->GetCurPage()?>"
    }
</script>

<?php

// var_dump($arResult["DATA"]);