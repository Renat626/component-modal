<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

    class modalComponent extends CBitrixComponent {
        public function renderForm() {
            if(!CModule::IncludeModule("form"))
	            return;

            $rsFields = CFormField::GetList(
                $this->arParams["FORM"],
                "Y"
            );

            $this->arResult["ITEMS"] = [];

            while ($arFields = $rsFields->Fetch()) {
                array_push($this->arResult["ITEMS"], $arFields);
            }
        }

        public function insertData() {
            if ($_POST) {
                $RESULT_ID = CFormResult::Add($this->arParams["FORM"]);

                foreach ($_POST as $key => $value) {
                    CFormResult::SetField(
                        $RESULT_ID,
                        $key,
                        $value
                    );
                }
            }
        }

        public function executeComponent() {
            $this->renderForm();
            $this->insertData();

            $this->includeComponentTemplate();
        }
    }
?>